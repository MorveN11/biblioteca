module Biblioteca.main {
  requires javafx.controls;
  requires javafx.fxml;
  requires java.desktop;
  requires javafx.media;
  requires org.json;
  requires json.simple;
  requires java.sql;
  exports programacion4.biblioteca.view.pagesMain;
  exports programacion4.biblioteca.controller;
  exports programacion4.biblioteca.model;

  opens programacion4.biblioteca.controller to javafx.fxml;
  opens programacion4.biblioteca.model to javafx.fxml;
  opens programacion4.biblioteca.view to javafx.fxml;
}
