package programacion4.biblioteca.model;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
public class WriteJSON {
  public static void initializeJson() {
    JSONObject jo = new JSONObject();
    /*jo.put("VerifyLogin", "false");

    Map c = new LinkedHashMap();
    jo.put("Codes", c);*/

    PrintWriter pw = null;
    try {
      pw = new PrintWriter("data.json");
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    }
    pw.write(jo.toString());
    pw.flush();
    pw.close();
  }

  public static void writeJson(String key, String text) {
    JSONObject existingData = readExistingJsonData();

    existingData.put(key, text);

    try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream("data.json"), StandardCharsets.UTF_8);
         BufferedWriter bw = new BufferedWriter(writer)) {
      existingData.write(bw);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static void writeJsonMap(String map, String key, String text) {
    JSONObject existingData = readExistingJsonData();

    JSONObject jo = existingData.optJSONObject(map);
    if (jo == null) {
      jo = new JSONObject();
      existingData.put(map, jo);
    }

    jo.put(key, text);

    try (PrintWriter pw = new PrintWriter("data.json")) {
      pw.write(existingData.toString());
      pw.flush();
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    }
  }

  private static JSONObject readExistingJsonData() {
    File file = new File("data.json");
    if (!file.exists()) {
      return new JSONObject();
    }

    try (FileInputStream fis = new FileInputStream(file);
         BufferedInputStream bis = new BufferedInputStream(fis)) {
      JSONTokener tokener = new JSONTokener(bis);
      return new JSONObject(tokener);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
