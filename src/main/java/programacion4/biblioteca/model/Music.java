package programacion4.biblioteca.model;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

public class Music {
  static MediaPlayer mediaPlayer;
  public static void playButton1() {
    File file = new File("src/main/java/programacion4/biblioteca/model/music/ButtonSound.mp3");
    Media media = new Media(file.toURI().toString());
    mediaPlayer = new MediaPlayer(media);
    mediaPlayer.setVolume(0.3);
    mediaPlayer.play();
  }

  public static void playButton2() {
    File file = new File("src/main/java/programacion4/biblioteca/model/music/ButtonSound2.mp3");
    Media media = new Media(file.toURI().toString());
    mediaPlayer = new MediaPlayer(media);
    mediaPlayer.setVolume(0.3);
    mediaPlayer.play();
  }

  public static void playButton3() {
    File file = new File("ssrc/main/java/programacion4/biblioteca/model/music/ButtonSound3.mp3");
    Media media = new Media(file.toURI().toString());
    mediaPlayer = new MediaPlayer(media);
    mediaPlayer.setVolume(1);
    mediaPlayer.play();
  }
}
