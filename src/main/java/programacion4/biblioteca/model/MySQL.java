package programacion4.biblioteca.model;

import java.sql.*;

public class MySQL {
  private static final String DB_URL = "jdbc:mysql://localhost:3306/players";
  private static final String DB_USER = "root";
  private static final String DB_PASSWORD = "1234";

  public static String getPlayerDataById(int playerId) {
    String info = null;
    try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
         Statement statement = connection.createStatement()) {
      ResultSet resultSet = statement.executeQuery("SELECT * FROM Player WHERE id = " + playerId);
      if (resultSet.next()) {
        info = String.valueOf(resultSet.getInt("id"));
        info += ("/" + resultSet.getString("name"));
        info += ("/" + resultSet.getString("email"));
        info += ("/" + resultSet.getString("phone"));
        info += ("/" + resultSet.getString("password"));
        info += ("/" + resultSet.getString("pathImage"));
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
    return info;
  }

  public static void deleteAllData() {
    try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
         Statement statement = connection.createStatement()) {
      statement.executeUpdate("DELETE FROM Player");
      System.out.println("Se ha eliminado toda la información de la tabla Player");
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }
}
