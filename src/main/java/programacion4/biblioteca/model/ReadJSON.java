package programacion4.biblioteca.model;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

public class ReadJSON {
  public static String readJSON(String id) {
    Object obj = null;
    try {
      obj = new JSONParser().parse(new FileReader("data.json"));
    } catch (IOException | ParseException e) {
      throw new RuntimeException(e);
    }
    JSONObject jsonObject = (JSONObject) obj;
    if (jsonObject.containsKey(id)) {
      return (String) jsonObject.get(id);
    } else {
      return null;
    }
  }

  public static String readJSONMap(String object, String id) {
    Object obj = null;
    try {
      obj = new JSONParser().parse(new FileReader("data.json"));
    } catch (IOException | ParseException e) {
      throw new RuntimeException(e);
    }
    JSONObject jsonObject = (JSONObject) obj;
    Map<?, ?> map = (Map<?, ?>) jsonObject.get(object);
    if (map != null && map.containsKey(id)) {
      return (String) map.get(id);
    } else {
      return null; // Devuelve null si el objeto o la clave no existen
    }
  }
}
