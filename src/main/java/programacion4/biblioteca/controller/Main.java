package programacion4.biblioteca.controller;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import programacion4.biblioteca.model.WriteJSON;
import programacion4.biblioteca.view.pagesMain.MainMenu;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Main extends Application {
  private static final String DB_URL = "jdbc:mysql://localhost:3306/players";
  private static final String DB_USER = "root";
  private static final String DB_PASSWORD = "1234";
  public static Stage gameStage;
  Scene scene;

  @Override
  public void start(Stage mainMenu) {
    //MySQL.deleteAllPlayerData();
    //createTableIfNotExists();
    //WriteJSON.initializeJson();
    Main.gameStage = mainMenu;
    MainMenu menuLayout = new MainMenu();
    menuLayout.build();

    Scene primaryScene = new Scene((Pane) menuLayout.getComponent(), 2000, 1024);
    this.scene = primaryScene;

    gameStage.setTitle("Tik Tak Toe");
    gameStage.setScene(primaryScene);
    gameStage.setResizable(false);
    gameStage.show();
  }
  private void createTableIfNotExists() {
    try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
         Statement statement = connection.createStatement()) {
      statement.executeUpdate("CREATE TABLE IF NOT EXISTS Player (id INT PRIMARY KEY, name VARCHAR(255), email VARCHAR(255), phone VARCHAR(20), password VARCHAR(255), pathImage VARCHAR(255))");
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }
}
