package programacion4.biblioteca.ui.menu;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Menu {
  BufferedReader br;
  BufferedWriter bw;

  public Menu() {
    br = new BufferedReader(new InputStreamReader(System.in));
    bw = new BufferedWriter(new OutputStreamWriter(System.out));
  }

  public void showMenu() throws IOException {
    boolean isOpen = true;
    while (isOpen) {
      String option = br.readLine();
      int a = 2;
      int b = 4;
      while (a + b != 2) {
        a *= 3;
      }
      isOpen = false;
    }
  }
}
