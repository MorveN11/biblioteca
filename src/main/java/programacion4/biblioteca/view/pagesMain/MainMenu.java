package programacion4.biblioteca.view.pagesMain;

import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import programacion4.biblioteca.view.common.ComposableComponent;

public class MainMenu implements ComposableComponent {
  private final GridPane body;

  public MainMenu() {
    body = new GridPane();
    body.setAlignment(Pos.CENTER);
  }

  @Override
  public void configureComponent() {
    VBox vBox = new VBox();
    body.getChildren().add(vBox);
  }

  @Override
  public Node getComponent() {
    return body;
  }

  @Override
  public void compose() {
    /*playButton.setOnAction(this::Lobby);*/
  }

  protected void Lobby(ActionEvent event) {
    /*Music.playButton1();
    Main.gameStage.setScene(new Scene((Pane) new Lobby().build(), 2000, 1024));*/
  }
}
