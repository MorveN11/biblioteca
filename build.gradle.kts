plugins {
    application
    id("org.openjfx.javafxplugin") version "0.0.14"
}

group = "programacion4.biblioteca"
version = "1.0-SNAPSHOT"

val javafxVersion = "16"

application {
    mainModule.set("programacion4.biblioteca")
    mainClass.set("programacion4.biblioteca.Main")
}

javafx {
    version = javafxVersion
    modules("javafx.controls", "javafx.fxml", "javafx.media")
}

repositories {
  mavenCentral()
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.webjars.npm:jsonparse:1.3.1")
    implementation("org.json:json:20220320")
    implementation("com.googlecode.json-simple:json-simple:1.1")
    implementation("com.googlecode.json-simple:json-simple:1.1.1")
    implementation("org.openjfx:javafx-graphics:${javafxVersion}")
}

tasks.test {
  useJUnitPlatform()
}
